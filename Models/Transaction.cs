﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessAPI.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public double Price { get; set; }
        public String TransactionDate { get; set; }
        public User UserPerform { get; set; }
        public Product OnProduct { get; set; }
    }
}
