﻿using BusinessAPI.ModelBinding;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessAPI.Models
{
    public class ProductImageRequestModel
    {
        [Required]
        public string DescPart { get; set; }

        [Required]
        public IFormFile Image { get; set; }
    }
}
