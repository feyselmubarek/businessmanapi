﻿using BusinessAPI.Models;
using BusinessAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessAPI.Repositories
{
    public class TransactionRepository : ITransactionRepository
    {
        private BusinessManDbContext _context;

        public TransactionRepository(BusinessManDbContext context)
        {
            _context = context;
        }

        public IQueryable<Transaction> GetAllTransaction()
        {
            return _context.Transactions;
        }

        public Transaction GetTransactionById(int id)
        {
            return _context.Transactions.FirstOrDefault(t => t.Id == id);
        }
    }
}
