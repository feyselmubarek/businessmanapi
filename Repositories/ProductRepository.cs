﻿using BusinessAPI.Models;
using BusinessAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessAPI.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private BusinessManDbContext _context;

        public ProductRepository(BusinessManDbContext context)
        {
            this._context = context;
        }

        public IQueryable<Product> GetAllProduct()
        {
            return _context.Products;
        }

        public Product GetProductbyId(int id)
        {
            return _context.Products.FirstOrDefault(p => p.Id == id);
        }


    }
}
