﻿using System.Linq;
using BusinessAPI.Models;

namespace BusinessAPI.Repositories
{
    public interface ITransactionRepository
    {
        IQueryable<Transaction> GetAllTransaction();
        Transaction GetTransactionById(int id);
    }
}