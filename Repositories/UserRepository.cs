﻿using BusinessAPI.Models;
using BusinessAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessAPI.Repositories
{
    public class UserRepository : IUserRepository
    {
        private BusinessManDbContext _context;

        public UserRepository(BusinessManDbContext context)
        {
            this._context = context;
        }

        public IQueryable<User> GetAllUser()
        {
            return _context.Users;
        }

        public User GetUserById(int id)
        {
            return _context.Users.FirstOrDefault(u => u.Id == id);
        }

        public void AddUser(User user)
        {
            _context.Users.Add(user);
        }

        public void DeleteUser(User user)
        {
            _context.Users.Remove(user);
        }
    }
}
