﻿using System.Linq;
using BusinessAPI.Models;

namespace BusinessAPI.Repositories
{
    public interface IProductRepository
    {
        IQueryable<Product> GetAllProduct();
        Product GetProductbyId(int id);
    }
}