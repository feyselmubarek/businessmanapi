﻿using System.Linq;
using BusinessAPI.Models;

namespace BusinessAPI.Repositories
{
    public interface IUserRepository
    {
        void AddUser(User user);
        void DeleteUser(User user);
        IQueryable<User> GetAllUser();
        User GetUserById(int id);
    }
}