﻿using BusinessAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessAPI.Utilities
{
    public class BusinessManDbContext : DbContext
    {
        public BusinessManDbContext(DbContextOptions<BusinessManDbContext> options): base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

    }
}
