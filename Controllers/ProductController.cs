﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BusinessAPI.Models;
using BusinessAPI.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BusinessAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly BusinessManDbContext _context;

        public ProductController(BusinessManDbContext context)
        {
            _context = context;
        }

        [HttpGet("user/{id}")]
        public IEnumerable<Product> GetRetailerProducts(int id)
        {
            var users = _context.Users;
            return _context.Products.Where(p => p.User.Id == id);
        }

        [HttpGet("{id}")]
        public Product GetProductById(int id)
        {
            return _context.Products.FirstOrDefault(p => p.Id == id);
        }

        [HttpPost("new")]
        public IActionResult AddNewProduct([FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exists = _context.Products.Any(p => p.ProductName == product.ProductName);

            if (exists)
            {
                return BadRequest("Product Already exists");
            }

            string base64 = product.ProductImageString.Split(',', 2)[0];
            string name = DateTime.Now.Ticks.ToString();

            var imageDatabyteArray = Convert.FromBase64String(base64);

            var imageDataStream = new MemoryStream(imageDatabyteArray);
            imageDataStream.Position = 0;

            FileStream F = new FileStream($@"C:\Users\Feysel\source\repos\BusinessAPI\BusinessAPI\wwwroot\Images\{name}.png",
                FileMode.OpenOrCreate, FileAccess.ReadWrite);
            F.Write(imageDatabyteArray, 0, imageDatabyteArray.Length);
            F.Close();

            var user_performing = _context.Users.FirstOrDefault(u => u.Id == product.User.Id);

            var new_product = new Product {
                ProductName = product.ProductName,
                ProductPrice = product.ProductPrice,
                ProductQuantity = product.ProductQuantity,
                ProductImageString = $"{name}.png",
                User = user_performing
            };

            _context.Products.Add(new_product);
            _context.SaveChanges();
            return Ok(new_product);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var voidProduct = new Product();
            Console.WriteLine();
            Console.WriteLine($"The id is {id}");
            Console.WriteLine();

            _context.Products.Remove(_context.Products.Single(p => p.Id == id));
            _context.SaveChanges();

            return Ok(voidProduct);
        }

        [HttpPut("update")]
        public IActionResult UpdateProduct([FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Product pr = _context.Products.SingleOrDefault(p => p.Id == product.Id);
            pr.ProductName = product.ProductName;
            pr.ProductPrice = product.ProductPrice;
            pr.ProductQuantity = product.ProductQuantity;
            _context.SaveChanges();

            return Ok(pr);
        }

        [HttpPost("upload")]
        public IActionResult UploadImage(ProductImageRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }           

            return Ok();
        }
    }
}