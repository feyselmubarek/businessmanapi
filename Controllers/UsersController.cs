﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessAPI.Models;
using BusinessAPI.Repositories;
using BusinessAPI.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BusinessAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _repo;

        public UsersController(IUserRepository repo)
        {
            this._repo = repo;
        }

        [HttpGet]
        public IEnumerable<User> GetAllUser()
        {
            return _repo.GetAllUser();
        }

        [HttpGet("{id}")]
        public User GetUserById(int id)
        {
            return _repo.GetUserById(id);
        }
    }
}