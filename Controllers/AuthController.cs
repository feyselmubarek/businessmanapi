﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessAPI.Models;
using BusinessAPI.Repositories;
using BusinessAPI.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BusinessAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly BusinessManDbContext _context;

        public AuthController(BusinessManDbContext context)
        {
            _context = context;
        }

        [HttpPost("login")]
        public IActionResult Login(User user)
        {
            //authenticate user

            var authUser = _context.Users.FirstOrDefault(u => u.UserName == user.UserName && u.Password == user.Password);
            if (authUser != null)
            {
                return Ok(authUser);
            }

            return Unauthorized();
        }

        [HttpPost("register")]
        public IActionResult Register([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var exists = _context.Users.Any(u => u.UserName == user.UserName);

            if (exists)
            {
                return BadRequest("Username is Already In Use");
            }

            var customer = new User { UserName = user.UserName, Password = user.Password, Name = user.Name };
            _context.Users.Add(customer);
            _context.SaveChanges();
                return Ok(customer);
        }
    }
}