﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessAPI.Models;
using BusinessAPI.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BusinessAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly BusinessManDbContext _context;

        public TransactionController(BusinessManDbContext context)
        {
            _context = context;
        }

        [HttpGet("user/{id}")]
        public IEnumerable<Transaction> GetTransactions(int id)
        {
            var users = _context.Users;
            return _context.Transactions.Where(p => p.UserPerform.Id == id);
        }

        [HttpPost("new")]
        public IActionResult AddNewTransaction([FromBody] Transaction transaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            User exists = _context.Users.FirstOrDefault(u => u.Id == transaction.UserPerform.Id);

            if (exists == null)
            {
                return BadRequest("User Not found");
            }

            Product pexists = _context.Products.FirstOrDefault(u => u.Id == transaction.OnProduct.Id);

            if (pexists == null)
            {
                return BadRequest("Product Not found");
            }

            var new_tranaction = new Transaction
            {
                OnProduct = transaction.OnProduct,
                Price = transaction.Price,
                TransactionDate = transaction.TransactionDate,
                UserPerform = transaction.UserPerform
            };

            _context.Transactions.Add(new_tranaction);
            _context.SaveChanges();

            return Ok(new_tranaction);
        }
    }
}